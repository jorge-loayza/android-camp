import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';

import { AppRoutingModule } from './app.routing';
import { DatosService } from './services/datos.service';
import { FireService } from './services/fire.service';
import { AuthService } from './services/auth.service';

import { NavbarComponent } from './components/navbar/navbar.component';
import { ListaInscritosComponent } from './components/lista-inscritos/lista-inscritos.component';
import { ListaPuntosComponent } from './components/lista-puntos/lista-puntos.component';
import { AcreditacionComponent } from './components/acreditacion/acreditacion.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FilterPipe } from './pipes/filter.pipe';

import { FormsModule } from '@angular/forms';

import { AuthGuard } from './guards/auth.guard';
import { GrupoComponent } from './components/grupo/grupo.component';
import { CanjePuntosComponent } from './components/canje-puntos/canje-puntos.component';
import { AcreditarGrupoComponent } from './components/acreditar-grupo/acreditar-grupo.component';
import { InscripcionComponent } from './components/inscripcion/inscripcion.component';
import { InfoComponent } from './components/info/info.component';
// Animacion
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ListaInscritosComponent,
    ListaPuntosComponent,
    AcreditacionComponent,
    FilterPipe,
    GrupoComponent,
    CanjePuntosComponent,
    AcreditarGrupoComponent,
    InscripcionComponent,
    InfoComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AppRoutingModule,
    NgbModule.forRoot()
  ],
  providers: [DatosService, FireService, AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
