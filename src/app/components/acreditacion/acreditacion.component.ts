import { Component, OnInit} from '@angular/core';
import { Inscrito } from './../../models/inscrito';
import { DatosService  } from './../../services/datos.service';
import { Router } from '@angular/router';

import { FireService } from './../../services/fire.service';
import { Observable } from 'rxjs/Observable';

import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-acreditacion',
  templateUrl: './acreditacion.component.html',
  styleUrls: ['./acreditacion.component.css']
})
export class AcreditacionComponent implements OnInit {
  // tamano de los grupos
  tamanoDeGrupo = 3;
  guias: any[] = [];
  guiaSelecionado;
  message: Inscrito;
  closeResult: string;
  modalOption: NgbModalOptions = {};
  seleccionado = false;
  searchText;

  constructor(private data: DatosService, private fire: FireService, private router: Router, private modalService: NgbModal) {
    this.data.currentMessage.subscribe(message => this.message = message);
    this.fire.getGuias().snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
      .subscribe(guias => {
        // this.guias = guias;
        this.guias = this.filtrar(guias);
      });
  }

  ngOnInit() {
  }
  filtrar(value) {
    const guiasFiltrados = [];
    value.forEach(guia => {
      if (guia.num_participantes < this.tamanoDeGrupo) {
        guiasFiltrados.push(guia);
      }
    });
    return guiasFiltrados;
  }
  seleccionarGuia(guia, $event?) {
    this.seleccionado = true;
    this.guiaSelecionado = guia;
  }
  guiaRandom() {
    this.guiaSelecionado = this.guias[Math.floor(Math.random() * this.guias.length)];
    // do {
    //   this.guiaSelecionado = items[Math.floor(Math.random() * items.length)];
    // } while (this.guiaSelecionado.num_participantes < this.tamanoDeGrupo);
    this.seleccionado = true;
  }
  verlista() {
    this.seleccionado = false;
  }
  open(content, inscrito, guia) {
    if (inscrito != null) {
      this.fire.acreditar(inscrito, guia);
      this.fire.agregarParticipante(guia);
      this.modalOption.backdrop = 'static';
      this.modalOption.keyboard = false;
      this.modalService.open(content, this.modalOption).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
        this.router.navigate(['acreditar']);
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        this.router.navigate(['acreditar']);
      });
    }else {
      console.log('hi!');
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
