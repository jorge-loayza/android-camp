import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaPuntosComponent } from './lista-puntos.component';

describe('ListaPuntosComponent', () => {
  let component: ListaPuntosComponent;
  let fixture: ComponentFixture<ListaPuntosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaPuntosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaPuntosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
