import { Component, OnInit } from '@angular/core';
import { FireService } from '../../services/fire.service';

@Component({
  selector: 'app-lista-puntos',
  templateUrl: './lista-puntos.component.html',
  styleUrls: ['./lista-puntos.component.css']
})
export class ListaPuntosComponent implements OnInit {

  acreditados: any[];

  constructor(private fire: FireService) {
    this.fire.getAcreditados().snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
      .subscribe(acreditados => {
        this.acreditados = acreditados;
        this.acreditados.reverse();
      });
   }

  ngOnInit() {
  }
}
