export class Acreditado {
    id: string;
    nombre: string;
    correo: string;
    genero: string;
    id_guia: string;
    nombre_guia: string;
    puntos: number;

    constructor(id: string, nombre: string, correo: string, genero: string, id_guia: string, nombre_guia: string) {
        this.id = id;
        this.nombre = nombre;
        this.correo = correo;
        this.genero = genero;
        this.id_guia = id_guia;
        this.nombre_guia = nombre_guia;
        this.puntos = 0;
    }

}
