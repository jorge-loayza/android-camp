import { Inscrito } from './../models/inscrito';
import { Injectable, group } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { Acreditado } from '../models/acreditado';
import { Ticket } from './../models/ticket';


@Injectable()
export class FireService {

  inscritos: AngularFireList<any[]>;
  guias: AngularFireList<any[]>;
  premios: AngularFireList<any[]>;
  usuario: AngularFireList<any[]>;
  constructor(private db: AngularFireDatabase) { }

  getInscritos() {
    this.inscritos = this.db.list('inscritos', ref => ref.orderByKey());
    return this.inscritos;
  }
  getGuias() {
    this.guias = this.db.list('guias', ref => ref.orderByChild('rol').equalTo(0));
    return this.guias;
  }
  getAcretidatoPuntos() {
    this.usuario = this.db.list('acreditado', ref => ref.orderByChild('puntos').limitToLast(1));
    return this.usuario;
  }
  getAcreditados(filtro?) {
    if (filtro) {
      this.inscritos = this.db.list('acreditado', ref => ref.orderByKey());
    }else {
      this.inscritos = this.db.list('acreditado', ref => ref.orderByChild('puntos').limitToLast(20));
    }
    return this.inscritos;
  }
  getAcreditados2(filtro) {
    if (filtro === 0) {
      this.inscritos = this.db.list('acreditado');
    }
    if (filtro === 1) {
      this.inscritos = this.db.list('acreditado', ref => ref.orderByChild('genero').equalTo('Femenino'));
    }
    if (filtro === 2) {
      this.inscritos = this.db.list('acreditado', ref => ref.orderByChild('genero').equalTo('Masculino'));
    }
    return this.inscritos;
  }
  getUsuario(id) {
    this.usuario = this.db.list('guias', ref => ref.orderByKey().equalTo(String(id)));
    return this.usuario;
  }
  getNumTicketsGuias() {
    this.guias = this.db.list('guias', ref => ref.orderByChild('num_tickets').limitToFirst(5));
    return this.guias;
  }
  acreditar(inscrito, guia) {
    const acreditado: Acreditado = new Acreditado(
      inscrito.id,
      inscrito.nombre,
      inscrito.correo,
      inscrito.genero,
      guia.id,
      guia.nombre
    );
    const acreditadoRef = this.db.list('acreditado');
    const inscritoRef = this.db.list('inscritos');
    acreditadoRef.set(String(acreditado.id), acreditado);
    inscritoRef.remove(String(acreditado.id));
  }
  getNumTickets() {
    this.inscritos = this.db.list('ticket');
    return this.inscritos;
  }
  getGrupo(id_guia) {
    this.inscritos = this.db.list('acreditado', ref => ref.orderByChild('id_guia').equalTo(id_guia));
    return this.inscritos;
  }
  agregarParticipante(guia) {
    this.db.list('guias').update(String(guia.id), { num_participantes: guia.num_participantes += 1 });
  }
  agregarTicket(inscrito, id) {
    this.db.list('acreditado').update(String(inscrito.id), { puntos: inscrito.puntos += 1 });
  }
  getPremios() {
    this.premios = this.db.list('premio');
    return this.premios;
  }
  canjearPuntos(premio) {
    const valor = premio.cantidad_disponible - (premio.cantidad);
    this.db.list('premio').update(String(premio.key), { cantidad_disponible: valor });
  }
  restarpuntos(acreditado, valor) {
    this.db.list('acreditado').update(String(acreditado.id), { puntos: valor });
  }
  verificaTicket(numero) {
    // return this.db.list('ticket', ref => ref.orderByChild('numero').equalTo(numero));
    return this.db.database.ref('ticket').orderByChild('numero').equalTo(numero);
  }
  getTickets(numero) {
    // return this.db.list('ticket', ref => ref.orderByChild('numero').equalTo(numero));
    return this.db.database.ref('ticket').orderByChild('numero').equalTo(numero);
  }
  agregarTicket2(guia, acreditado, numero) {
    const ticket = new Ticket();
    ticket.id_acreditado = acreditado.id;
    ticket.id_guia = guia.id;
    ticket.nombre_acreditado = acreditado.nombre;
    ticket.nombre_guia = guia.nombre;
    ticket.numero = numero;
    this.db.list('ticket').push(ticket);
    // sumar el valor de un ticket a los puntos
    this.db.list('acreditado').update(String(acreditado.id), { puntos: acreditado.puntos += 5 });
  }
  inscribir(inscrito): Inscrito {
    if (inscrito != null) {
      this.db.database.ref('inscritos').orderByKey().limitToLast(1)
        .once('value').then((snap) => {
          const ins = snap.val()[Object.keys(snap.val())[0]];
          inscrito.id = ins.id + 1;
          this.db.list('inscritos').set(String(inscrito.id), inscrito);
          console.log(ins);
          console.log(inscrito);
          return inscrito;
        });
    }else {
      return null;
    }
  }
}
